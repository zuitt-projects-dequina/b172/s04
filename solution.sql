-- S04 Activity Code

-- a. finding all artist with D in its name
SELECT * FROM artists
WHERE name LIKE "%d%";

-- b. finding all songs that has <230 length
SELECT * FROM songs
WHERE length < 230;

-- c. Join the "albums" and "songs" tables. (ONLY album_name, song_name, song_length)
SELECT albums.album_title, songs.song_name, songs.length
    FROM songs
    JOIN albums ON albums.id = songs.album_id;

-- d. Join the 'artists' and 'albums' tables. (find all albums that has letter 'A' in its name)
SELECT *
    FROM artists
    JOIN albums ON albums.artist_id = artists.id
    WHERE album_title LIKE "%a%";

-- e. Sort the albums in Z-A order. (show only first 4 records)
SELECT album_title
    FROM albums 
    ORDER BY  album_title DESC
    LIMIT 4;

-- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z)
SELECT album_title, song_name 
    FROM songs
    JOIN albums ON albums.id = songs.album_id
    ORDER BY album_title DESC, song_name ASC;